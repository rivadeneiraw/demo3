package com.qaagility.controller;

public class Counter {

    public int dividir(int dividendo, int divisor) {
        if (divisor == 0) {
            return Integer.MAX_VALUE;
        }
        else {
            return dividendo / divisor;
        }
    }

}
